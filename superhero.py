import csv
import shutil
import os
import sys
import subprocess
from sqlalchemy import create_engine
import rs_engine_config
import pandas as pd

encoding='utf-8'
rs_engine = create_engine(rs_engine_config.rs_engine_str)


# copy orgin csv to a temp csv file which cut the head column
def get_csv_to_temp(tablename):
    with open('archive/%s.csv' % tablename, 'r',errors='ignore') as r:
        lines = r.readlines()
    with open('temp_csv/%s.csv' % tablename, 'w',errors='ignore') as w:
        key = 0
        for l in lines:
            if (key != 0):
                w.write(l)
                key += 1
            else:
                key += 1

# load temp csv data into Redshift database
def csv_to_df(tablename,labels):
    df = pd.read_csv('temp_csv/%s.csv'%tablename, names=labels)
    rs_engine.execute('truncate %s;'%tablename)
    df.to_sql(tablename, con=rs_engine, if_exists="append", index=False)

# delete temp csv file after we use it
def remove_temp_csv(tablename):
    os.remove('temp_csv/%s.csv' % tablename)




# the whole process to load csv to a staging table
def get_sta_table(tablename,characters_label):
    get_csv_to_temp(tablename)
    csv_to_df(tablename,characters_label)
    remove_temp_csv(tablename)

# get all 7 staging tables data
def get_sta():
    characters_label = ['characterID', 'name']
    characterstocomics_label = ['comicID', 'characterID']
    charcters_stats_label = ['name', 'alignment', 'intelligence', 'strength', 'speed', 'durability', 'power', 'combat',
                             'total']
    comics_label = ['comicid', 'title', 'issuenumber', 'description']
    marvel_characters_info_label = ['id', 'name', 'alignment', 'gender', 'eyecolor', 'race', 'haircolor', 'publisher',
                                    'skincolor', 'height', 'weight']
    marvel_dc_characters_label = ['id', 'name', 'vc_identity', 'alignment', 'eyecolor', 'haircolor', 'gender', 'status',
                                  'appearances', 'firstappearance', 'year', 'universe']
    superheroes_power_matrix_label = ['name', 'agility', 'accelerated healing', 'lantern power ring',
                                      'dimensional awareness', 'cold resistance', 'durability', 'stealth',
                                      'energy absorption', 'flight', 'danger sense', 'underwater breathing',
                                      'marksmanship', 'weapons master', 'power augmentation', 'animal attributes',
                                      'longevity', 'intelligence', 'super strength', 'cryokinesis', 'telepathy',
                                      'energy armor', 'energy blasts', 'duplication', 'size changing',
                                      'density control', 'stamina', 'astral travel', 'audio control', 'dexterity',
                                      'omnitrix', 'super speed', 'possession', 'animal oriented powers',
                                      'weapon-based powers', 'electrokinesis', 'darkforce manipulation', 'death touch',
                                      'teleportation', 'enhanced senses', 'telekinesis', 'energy beams', 'magic',
                                      'hyperkinesis', 'jump', 'clairvoyance', 'dimensional travel', 'power sense',
                                      'shapeshifting', 'peak human condition', 'immortality', 'camouflage',
                                      'element control', 'phasing', 'astral projection', 'electrical transport',
                                      'fire control', 'projection', 'summoning', 'enhanced memory', 'reflexes',
                                      'invulnerability', 'energy constructs', 'force fields', 'self-sustenance',
                                      'anti-gravity', 'empathy', 'power nullifier', 'radiation control',
                                      'psionic powers', 'elasticity', 'substance secretion',
                                      'elemental transmogrification', 'technopath/cyberpath', 'photographic reflexes',
                                      'seismic power', 'animation', 'precognition', 'mind control', 'fire resistance',
                                      'power absorption', 'enhanced hearing', 'nova force', 'insanity', 'hypnokinesis',
                                      'animal control', 'natural armor', 'intangibility', 'enhanced sight',
                                      'molecular manipulation', 'heat generation', 'adaptation', 'gliding',
                                      'power suit', 'mind blast', 'probability manipulation', 'gravity control',
                                      'regeneration', 'light control', 'echolocation', 'levitation',
                                      'toxin and disease control', 'banish', 'energy manipulation', 'heat resistance',
                                      'natural weapons', 'time travel', 'enhanced smell', 'illusions', 'thirstokinesis',
                                      'hair manipulation', 'illumination', 'omnipotent', 'cloaking', 'changing armor',
                                      'power cosmic', 'biokinesis', 'water control', 'radiation immunity',
                                      'vision - telescopic', 'toxin and disease resistance', 'spatial awareness',
                                      'energy resistance', 'telepathy resistance', 'molecular combustion',
                                      'omnilingualism', 'portal creation', 'magnetism', 'mind control resistance',
                                      'plant control', 'sonar', 'sonic scream', 'time manipulation', 'enhanced touch',
                                      'magic resistance', 'invisibility', 'sub-mariner', 'radiation absorption',
                                      'intuitive aptitude', 'vision - microscopic', 'melting', 'wind control',
                                      'super breath', 'wallcrawling', 'vision - night', 'vision - infrared',
                                      'grim reaping', 'matter absorption', 'the force', 'resurrection', 'terrakinesis',
                                      'vision - heat', 'vitakinesis', 'radar sense', 'qwardian power ring',
                                      'weather control', 'vision - x-ray', 'vision - thermal', 'web creation',
                                      'reality warping', 'odin force', 'symbiote costume', 'speed force',
                                      'phoenix force', 'molecular dissipation', 'vision - cryo', 'omnipresent',
                                      'omniscient']
    get_sta_table('characters',characters_label)
    get_sta_table('characterstocomics', characterstocomics_label)
    get_sta_table('charcters_stats', charcters_stats_label)
    get_sta_table('comics', comics_label)
    get_sta_table('marvel_characters_info', marvel_characters_info_label)
    get_sta_table('marvel_dc_characters', marvel_dc_characters_label)
    get_sta_table('superheroes_power_matrix', superheroes_power_matrix_label)







# run superhero.sql to get result table t_com_fact_super_hero
def run_sql_script():
    try:
        with open('script/superhero.sql',"r",encoding='UTF-8',errors='ignore') as f:
           sql=f.read()
        #print(sql)
        rs_engine.execute(sql)

    except Exception as err:
        print(err)

# store data in t_com_fact_super_hero to json file
def covert_to_json():
    sql_string="SELECT * FROM t_com_fact_super_hero"
    df = pd.read_sql_query(sql_string,rs_engine,index_col=None, coerce_float=True, params=None, parse_dates=None,chunksize=None).fillna('')
    with open('result/superhero.json', 'w') as w:
        for idx, row in df.iterrows():
            print('---------------------------------------')
            characterid = row[0]
            name = row[1].replace("'","")
            alignment = row[2]
            intelligence = row[3]
            strength = row[4]
            speed = row[5]
            durability = row[6]
            power = row[7]
            combat = row[8]
            total = row[9]
            comics_info = row[10]
            eyecolor = row[11]
            haircolor = row[12]
            gender = row[13]
            race = row[14]
            publisher = row[15]
            skincolor = row[16]
            height = row[17]
            weight = row[18]
            identity = row[19]
            status = row[20]
            appearances = row[21]
            firstappearance = row[22]
            year = row[23]
            universe = row[24]
            super_power = row[25][:-1]
            json_line = """{'characterid':'%s','name':'%s','alignment':'%s','intelligence':'%s','strength':'%s','speed':'%s','durability':'%s','power':'%s','combat':'%s','total':'%s','comics_info':{%s},'eyecolor':'%s','haircolor':'%s','gender':'%s','race':'%s','publisher':'%s','skincolor':'%s','height':'%s','weight':'%s','identity':'%s','status':'%s','appearances':'%s','firstappearance':'%s','year':'%s','universe':'%s','super_power':{%s}}\n""" % (
            characterid, name, alignment, intelligence, strength, speed, durability, power, combat, total, comics_info,
            eyecolor, haircolor, gender, race, publisher, skincolor, height, weight, identity, status, appearances,
            firstappearance, year, universe, super_power)
            print(json_line)
            try:
                w.write(json_line)
            except Exception as err:
                print(err)




if __name__ == "__main__":
    get_sta()
    run_sql_script()
    covert_to_json()
