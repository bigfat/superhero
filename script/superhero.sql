TRUNCATE t_com_fact_super_hero;
INSERT into t_com_fact_super_hero
(i_characterid,vc_name,vc_alignment,i_intelligence,i_strength,i_speed,i_durability,i_power,i_combat,i_total,vc_comics_info,vc_eyecolor,vc_haircolor,vc_gender,vc_race,vc_publisher,vc_skincolor,n_height,n_weight,vc_identity,vc_status,vc_appearances,vc_firstappearance,i_year,vc_universe,vc_super_power)
WITH vw_charcters_stats as (
SELECT * 
FROM (
SELECT 	name,
				alignment,
				intelligence,
				strength,
				speed,
				durability,
				power,
				combat,
				total,
				ROW_NUMBER() over (PARTITION by name) as n_seq
FROM charcters_stats
) A
WHERE n_seq=1
),

vw_comics as (
SELECT 	characterID,
				listagg(vc_value,',') WITHIN GROUP (ORDER BY comicID) as vc_value
FROM (
SELECT 	DISTINCT a.characterID,
				a.comicID,
				''''||b.comicID||'''' as vc_value
FROM charactersToComics a
LEFT JOIN comics b on a.comicID=b.comicID
) A 
GROUP BY characterID
),
vw_marvel_dc_info as (
SELECT 	id,
				'marvel' as comic_company,
				name,
				alignment,		
				eyecolor,
				haircolor,
				gender,
				race,
				publisher,
				skincolor,
				height,
				weight,	
				null as vc_identity,
				null as status,
				null as appearances,
				null as firstappearance,
				null as year,
				null as universe
FROM marvel_characters_info
union all
SELECT 	id,
				'dc' as comic_company,
				name,
				alignment,		
				eyecolor,
				haircolor,
				gender,	
				null as race,
				null as publisher,
				null as skincolor,
				null as height,
				null as weight,			
				vc_identity,
				status,
				appearances,
				firstappearance,
				year,
				universe
FROM marvel_dc_characters
), vw_marvel_dc_info_real as (
SELECT * FROM (
SELECT *,ROW_NUMBER() over (PARTITION by name) as n_seq
FROM vw_marvel_dc_info
) A 
WHERE n_seq=1
), vw_super_power as (
SELECT name,
case when "agility"='TRUE' then '''agility'',' else '' end||
case when "accelerated healing"='TRUE' then '''accelerated healing'',' else '' end||
case when "lantern power ring"='TRUE' then '''lantern power ring'',' else '' end||
case when "dimensional awareness"='TRUE' then '''dimensional awareness'',' else '' end||
case when "cold resistance"='TRUE' then '''cold resistance'',' else '' end||
case when "durability"='TRUE' then '''durability'',' else '' end||
case when "stealth"='TRUE' then '''stealth'',' else '' end||
case when "energy absorption"='TRUE' then '''energy absorption'',' else '' end||
case when "flight"='TRUE' then '''flight'',' else '' end||
case when "danger sense"='TRUE' then '''danger sense'',' else '' end||
case when "underwater breathing"='TRUE' then '''underwater breathing'',' else '' end||
case when "marksmanship"='TRUE' then '''marksmanship'',' else '' end||
case when "weapons master"='TRUE' then '''weapons master'',' else '' end||
case when "power augmentation"='TRUE' then '''power augmentation'',' else '' end||
case when "animal attributes"='TRUE' then '''animal attributes'',' else '' end||
case when "longevity"='TRUE' then '''longevity'',' else '' end||
case when "intelligence"='TRUE' then '''intelligence'',' else '' end||
case when "super strength"='TRUE' then '''super strength'',' else '' end||
case when "cryokinesis"='TRUE' then '''cryokinesis'',' else '' end||
case when "telepathy"='TRUE' then '''telepathy'',' else '' end||
case when "energy armor"='TRUE' then '''energy armor'',' else '' end||
case when "energy blasts"='TRUE' then '''energy blasts'',' else '' end||
case when "duplication"='TRUE' then '''duplication'',' else '' end||
case when "size changing"='TRUE' then '''size changing'',' else '' end||
case when "density control"='TRUE' then '''density control'',' else '' end||
case when "stamina"='TRUE' then '''stamina'',' else '' end||
case when "astral travel"='TRUE' then '''astral travel'',' else '' end||
case when "audio control"='TRUE' then '''audio control'',' else '' end||
case when "dexterity"='TRUE' then '''dexterity'',' else '' end||
case when "omnitrix"='TRUE' then '''omnitrix'',' else '' end||
case when "super speed"='TRUE' then '''super speed'',' else '' end||
case when "possession"='TRUE' then '''possession'',' else '' end||
case when "animal oriented powers"='TRUE' then '''animal oriented powers'',' else '' end||
case when "weapon-based powers"='TRUE' then '''weapon-based powers'',' else '' end||
case when "electrokinesis"='TRUE' then '''electrokinesis'',' else '' end||
case when "darkforce manipulation"='TRUE' then '''darkforce manipulation'',' else '' end||
case when "death touch"='TRUE' then '''death touch'',' else '' end||
case when "teleportation"='TRUE' then '''teleportation'',' else '' end||
case when "enhanced senses"='TRUE' then '''enhanced senses'',' else '' end||
case when "telekinesis"='TRUE' then '''telekinesis'',' else '' end||
case when "energy beams"='TRUE' then '''energy beams'',' else '' end||
case when "magic"='TRUE' then '''magic'',' else '' end||
case when "hyperkinesis"='TRUE' then '''hyperkinesis'',' else '' end||
case when "jump"='TRUE' then '''jump'',' else '' end||
case when "clairvoyance"='TRUE' then '''clairvoyance'',' else '' end||
case when "dimensional travel"='TRUE' then '''dimensional travel'',' else '' end||
case when "power sense"='TRUE' then '''power sense'',' else '' end||
case when "shapeshifting"='TRUE' then '''shapeshifting'',' else '' end||
case when "peak human condition"='TRUE' then '''peak human condition'',' else '' end||
case when "immortality"='TRUE' then '''immortality'',' else '' end||
case when "camouflage"='TRUE' then '''camouflage'',' else '' end||
case when "element control"='TRUE' then '''element control'',' else '' end||
case when "phasing"='TRUE' then '''phasing'',' else '' end||
case when "astral projection"='TRUE' then '''astral projection'',' else '' end||
case when "electrical transport"='TRUE' then '''electrical transport'',' else '' end||
case when "fire control"='TRUE' then '''fire control'',' else '' end||
case when "projection"='TRUE' then '''projection'',' else '' end||
case when "summoning"='TRUE' then '''summoning'',' else '' end||
case when "enhanced memory"='TRUE' then '''enhanced memory'',' else '' end||
case when "reflexes"='TRUE' then '''reflexes'',' else '' end||
case when "invulnerability"='TRUE' then '''invulnerability'',' else '' end||
case when "energy constructs"='TRUE' then '''energy constructs'',' else '' end||
case when "force fields"='TRUE' then '''force fields'',' else '' end||
case when "self-sustenance"='TRUE' then '''self-sustenance'',' else '' end||
case when "anti-gravity"='TRUE' then '''anti-gravity'',' else '' end||
case when "empathy"='TRUE' then '''empathy'',' else '' end||
case when "power nullifier"='TRUE' then '''power nullifier'',' else '' end||
case when "radiation control"='TRUE' then '''radiation control'',' else '' end||
case when "psionic powers"='TRUE' then '''psionic powers'',' else '' end||
case when "elasticity"='TRUE' then '''elasticity'',' else '' end||
case when "substance secretion"='TRUE' then '''substance secretion'',' else '' end||
case when "elemental transmogrification"='TRUE' then '''elemental transmogrification'',' else '' end||
case when "technopath/cyberpath"='TRUE' then '''technopath/cyberpath'',' else '' end||
case when "photographic reflexes"='TRUE' then '''photographic reflexes'',' else '' end||
case when "seismic power"='TRUE' then '''seismic power'',' else '' end||
case when "animation"='TRUE' then '''animation'',' else '' end||
case when "precognition"='TRUE' then '''precognition'',' else '' end||
case when "mind control"='TRUE' then '''mind control'',' else '' end||
case when "fire resistance"='TRUE' then '''fire resistance'',' else '' end||
case when "power absorption"='TRUE' then '''power absorption'',' else '' end||
case when "enhanced hearing"='TRUE' then '''enhanced hearing'',' else '' end||
case when "nova force"='TRUE' then '''nova force'',' else '' end||
case when "insanity"='TRUE' then '''insanity'',' else '' end||
case when "hypnokinesis"='TRUE' then '''hypnokinesis'',' else '' end||
case when "animal control"='TRUE' then '''animal control'',' else '' end||
case when "natural armor"='TRUE' then '''natural armor'',' else '' end||
case when "intangibility"='TRUE' then '''intangibility'',' else '' end||
case when "enhanced sight"='TRUE' then '''enhanced sight'',' else '' end||
case when "molecular manipulation"='TRUE' then '''molecular manipulation'',' else '' end||
case when "heat generation"='TRUE' then '''heat generation'',' else '' end||
case when "adaptation"='TRUE' then '''adaptation'',' else '' end||
case when "gliding"='TRUE' then '''gliding'',' else '' end||
case when "power suit"='TRUE' then '''power suit'',' else '' end||
case when "mind blast"='TRUE' then '''mind blast'',' else '' end||
case when "probability manipulation"='TRUE' then '''probability manipulation'',' else '' end||
case when "gravity control"='TRUE' then '''gravity control'',' else '' end||
case when "regeneration"='TRUE' then '''regeneration'',' else '' end||
case when "light control"='TRUE' then '''light control'',' else '' end||
case when "echolocation"='TRUE' then '''echolocation'',' else '' end||
case when "levitation"='TRUE' then '''levitation'',' else '' end||
case when "toxin and disease control"='TRUE' then '''toxin and disease control'',' else '' end||
case when "banish"='TRUE' then '''banish'',' else '' end||
case when "energy manipulation"='TRUE' then '''energy manipulation'',' else '' end||
case when "heat resistance"='TRUE' then '''heat resistance'',' else '' end||
case when "natural weapons"='TRUE' then '''natural weapons'',' else '' end||
case when "time travel"='TRUE' then '''time travel'',' else '' end||
case when "enhanced smell"='TRUE' then '''enhanced smell'',' else '' end||
case when "illusions"='TRUE' then '''illusions'',' else '' end||
case when "thirstokinesis"='TRUE' then '''thirstokinesis'',' else '' end||
case when "hair manipulation"='TRUE' then '''hair manipulation'',' else '' end||
case when "illumination"='TRUE' then '''illumination'',' else '' end||
case when "omnipotent"='TRUE' then '''omnipotent'',' else '' end||
case when "cloaking"='TRUE' then '''cloaking'',' else '' end||
case when "changing armor"='TRUE' then '''changing armor'',' else '' end||
case when "power cosmic"='TRUE' then '''power cosmic'',' else '' end||
case when "biokinesis"='TRUE' then '''biokinesis'',' else '' end||
case when "water control"='TRUE' then '''water control'',' else '' end||
case when "radiation immunity"='TRUE' then '''radiation immunity'',' else '' end||
case when "vision - telescopic"='TRUE' then '''vision - telescopic'',' else '' end||
case when "toxin and disease resistance"='TRUE' then '''toxin and disease resistance'',' else '' end||
case when "spatial awareness"='TRUE' then '''spatial awareness'',' else '' end||
case when "energy resistance"='TRUE' then '''energy resistance'',' else '' end||
case when "telepathy resistance"='TRUE' then '''telepathy resistance'',' else '' end||
case when "molecular combustion"='TRUE' then '''molecular combustion'',' else '' end||
case when "omnilingualism"='TRUE' then '''omnilingualism'',' else '' end||
case when "portal creation"='TRUE' then '''portal creation'',' else '' end||
case when "magnetism"='TRUE' then '''magnetism'',' else '' end||
case when "mind control resistance"='TRUE' then '''mind control resistance'',' else '' end||
case when "plant control"='TRUE' then '''plant control'',' else '' end||
case when "sonar"='TRUE' then '''sonar'',' else '' end||
case when "sonic scream"='TRUE' then '''sonic scream'',' else '' end||
case when "time manipulation"='TRUE' then '''time manipulation'',' else '' end||
case when "enhanced touch"='TRUE' then '''enhanced touch'',' else '' end||
case when "magic resistance"='TRUE' then '''magic resistance'',' else '' end||
case when "invisibility"='TRUE' then '''invisibility'',' else '' end||
case when "sub-mariner"='TRUE' then '''sub-mariner'',' else '' end||
case when "radiation absorption"='TRUE' then '''radiation absorption'',' else '' end||
case when "intuitive aptitude"='TRUE' then '''intuitive aptitude'',' else '' end||
case when "vision - microscopic"='TRUE' then '''vision - microscopic'',' else '' end||
case when "melting"='TRUE' then '''melting'',' else '' end||
case when "wind control"='TRUE' then '''wind control'',' else '' end||
case when "super breath"='TRUE' then '''super breath'',' else '' end||
case when "wallcrawling"='TRUE' then '''wallcrawling'',' else '' end||
case when "vision - night"='TRUE' then '''vision - night'',' else '' end||
case when "vision - infrared"='TRUE' then '''vision - infrared'',' else '' end||
case when "grim reaping"='TRUE' then '''grim reaping'',' else '' end||
case when "matter absorption"='TRUE' then '''matter absorption'',' else '' end||
case when "the force"='TRUE' then '''the force'',' else '' end||
case when "resurrection"='TRUE' then '''resurrection'',' else '' end||
case when "terrakinesis"='TRUE' then '''terrakinesis'',' else '' end||
case when "vision - heat"='TRUE' then '''vision - heat'',' else '' end||
case when "vitakinesis"='TRUE' then '''vitakinesis'',' else '' end||
case when "radar sense"='TRUE' then '''radar sense'',' else '' end||
case when "qwardian power ring"='TRUE' then '''qwardian power ring'',' else '' end||
case when "weather control"='TRUE' then '''weather control'',' else '' end||
case when "vision - x-ray"='TRUE' then '''vision - x-ray'',' else '' end||
case when "vision - thermal"='TRUE' then '''vision - thermal'',' else '' end||
case when "web creation"='TRUE' then '''web creation'',' else '' end||
case when "reality warping"='TRUE' then '''reality warping'',' else '' end||
case when "odin force"='TRUE' then '''odin force'',' else '' end||
case when "symbiote costume"='TRUE' then '''symbiote costume'',' else '' end||
case when "speed force"='TRUE' then '''speed force'',' else '' end||
case when "phoenix force"='TRUE' then '''phoenix force'',' else '' end||
case when "molecular dissipation"='TRUE' then '''molecular dissipation'',' else '' end||
case when "vision - cryo"='TRUE' then '''vision - cryo'',' else '' end||
case when "omnipresent"='TRUE' then '''omnipresent'',' else '' end||
case when "omniscient"='TRUE' then '''omniscient'',' else '' end
AS VC_VALUE
FROM superheroes_power_matrix
)
SELECT 	characters.characterID,
				characters.name,				
				charcters_stats.alignment,
				charcters_stats.intelligence,
				charcters_stats.strength,
				charcters_stats.speed,
				charcters_stats.durability,
				charcters_stats.power,
				charcters_stats.combat,
				charcters_stats.total,
				comics.vc_value as comics_info,
				marvel_dc_info.eyecolor,
				marvel_dc_info.haircolor,
				marvel_dc_info.gender,	
				marvel_dc_info.race,
				marvel_dc_info.publisher,
				marvel_dc_info.skincolor,
				marvel_dc_info.height,
				marvel_dc_info.weight,			
				marvel_dc_info.vc_identity,
				marvel_dc_info.status,
				marvel_dc_info.appearances,
				marvel_dc_info.firstappearance,
				marvel_dc_info.year,
				marvel_dc_info.universe,
				super_power.vc_value as super_power
FROM characters as characters
LEFT JOIN vw_charcters_stats charcters_stats on charcters_stats.name=characters.name
LEFT JOIN vw_comics comics on comics.characterID=characters.characterID
LEFT JOIN vw_marvel_dc_info_real marvel_dc_info on marvel_dc_info.name=characters.name
LEFT JOIN vw_super_power super_power on super_power.name=characters.name;