import csv
import shutil
import os
import sys
import subprocess

def get_csv_to_temp2(tablename):
    with open('archive/%s.csv'%tablename, 'r') as r:
        lines = r.readlines()
    with open('temp_csv/temp_csv.csv', 'w') as w:
        key=0
        for l in lines:
            if(key!=0):
                w.write(l)
                key+=1
            else:
                key+=1

def remove_temp_csv(tablename):
    os.remove('temp_csv/%s.csv'%tablename)

def load_into_db(tablename):
    schema_name='atzc_poc'


    #copy to s3
    shell_command = "aws s3 cp /data/etl_log/csv/%s.csv s3://atzuchepoc/poc/temp/"
    os.system(shell_command)
    print("Finish copy csv to s3!")

    sql = """
    truncate %s.%s;
    copy %s.%s from 's3://atzuchepoc/poc/temp/%s.csv' 
    iam_role 'arn:aws-cn:iam::967115490315:role/ATZC_Redshift_Read_S3'
    delimiter ','
    NULL AS 'null_string' 
    COMPUPDATE OFF STATUPDATE OFF;""" % (schema_name, tablename, tablename)


if __name__ == "__main__":
    get_csv_to_temp('characters')
    #delete_head('characters')
    #remove_temp_csv('characters')