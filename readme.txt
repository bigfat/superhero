Develop a solution that seeks to join related data together into document-based data structure, saving this to a suitable format (e.g. JSON, AVRO, Parquet) and load into a database of your choice (SQLSQL/SparkSQL/Presto) hosted on Docker.  You may use any programming language of choice for ETL. You will be required to discuss and answer questions about your solution, approach and challenges faced.
 
You can also spend as much or as little time as you feel necessary.
 
Please use Git for your solution and zip up a copy of the repository 7 days after receiving this task. If this is not possible, please email the solution.
 
The candidate should assume we will re-build the solution our end.